<?php
/**
 * @file
 * banner_rotator.features.inc
 */

/**
 * Implements hook_views_api().
 */
function banner_rotator_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function banner_rotator_node_info() {
  $items = array(
    'banner_rotator' => array(
      'name' => t('Banner rotator'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
